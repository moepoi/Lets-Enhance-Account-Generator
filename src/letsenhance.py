import requests, json, sys, random, string

class LetsEnhance(object):
    
    def __init__(self):
        self.url = "https://letsenhance.io"
        self.rq = requests.Session()
        self.proxies = {
            "http": "" # Optional
        }
        #self.rq.proxies = self.proxies
        self.rq.headers.update({'Cache-Control': 'no-cache, no-store, must-revalidate'})
        self.email = ''.join(random.choice(string.ascii_lowercase) for i in range(6)) + '@opten.email'
        self.passwd = ''.join(random.choice(string.ascii_lowercase) for i in range(10))
        
    def register(self):
        headers = {
            'authority': 'letsenhance.io',
            'accept': 'application/json, text/plain, */*',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36',
            'content-type': 'application/json;charset=UTF-8',
            'origin': self.url,
            'sec-fetch-site': 'same-origin',
            'sec-fetch-mode': 'cors',
            'sec-fetch-dest': 'empty',
            'referer': self.url + '/signup',
            'accept-language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7'
        }
        data = '{"email":"{}","password":"{}","email_subscriber":false,"terms_agreed":true}'.format(self.email, self.passwd)
        res = json.loads(self.rq.post(self.url + '/api/v1/auth/signup', headers=headers, data=data).text)
        if "id" in res:
            return res
        else:
            print ("Invalid email address.")
            sys.exit()
            
    def resend_verification(self):
        headers = {
            'authority': 'letsenhance.io',
            'accept': 'application/json, text/plain, */*',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36',
            'content-type': 'application/json;charset=UTF-8',
            'origin': self.url,
            'sec-fetch-site': 'same-origin',
            'sec-fetch-mode': 'cors',
            'sec-fetch-dest': 'empty',
            'referer': self.url + '/login',
            'accept-language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7'
        }
        data = '{"email":"{}"}'.format(self.email)
        res = requests.post(self.url + '/api/v1/auth/resend-confirmation', headers=headers, data=data)
        return res
        
    def confirm_verification(self, token=None):
        res = requests.post(self.url + '/api/v1/auth/confirm_and_login/{}'.format(str(token)))
        return res
        
    def run(self):
        self.register()
        #get_token()
        self.resend_verification()
        self.confirm_verification()